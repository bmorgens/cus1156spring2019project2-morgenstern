package main;
import java.io.FileNotFoundException;
import java.time.LocalDate;

import election.*;
import electioncomponents.*;
import fromfile.VoterFromFile;

public class RunElections {
	public static void main(String[] args) {
		Election primary = new PrimaryElection(Party.DEMOCRAT, LocalDate.of(2020, 9, 15),"OH");
		Election presidential = new PresidentialElection(LocalDate.of(2020, 11, 3),"OH");
		VoterRegistry vreg;
		vreg = registerVoters("Voters.txt");
		if(vreg == null){
			System.err.println("Voter Registry could not be created; exiting program");
			return;
		}
		
		System.out.println("All Voters:");
		for (Voter voter : vreg.getSortedList()) 
			System.out.println(voter.name());
		System.out.println();
		
		ElectionThread prim = new ElectionThread(primary, vreg);
		ElectionThread pres = new ElectionThread(presidential, vreg);
		System.out.println(String.format("setting up Democratic primary for %s\n", primary.getDateOfElection()));
		prim.start();
		System.out.println(String.format("setting up Presidential election for %s\n", presidential.getDateOfElection()));
		pres.start();
		
		try {
			prim.join();
			pres.join();
		}catch (InterruptedException e) {
			e.printStackTrace();
		} 
		
		if(prim.succeeded()) {
			primary.tabulate();
			System.out.println("the winner of the primary is " + primary.getWinner().getLastName());
			System.out.println("total number of votes cast : " + primary.totalNumberOfVotes());
		}else {
			System.out.println(String.format("%s election could not be run", primary.getTypeLabel()));
		}
		
		
    	if(pres.succeeded()) {
			presidential.tabulate();
			System.out.println("the winner of the presidential election is " + presidential.getWinner().getLastName());
			presidential.printCandidateTotals();
    	}
    	else {
			System.out.println(String.format("%s election could not be run", presidential.getTypeLabel()));
		}
	}
	
	/**
	 * Create a VoterRegistry from a file of Voters
	 * @param fileName File to read Voters data from
	 * @return returns populated VoteryRegistry
	 */
	private static VoterRegistry registerVoters(String fileName){
		VoterRegistry vreg = new VoterRegistry();
		try {
			new VoterFromFile(fileName).registerVoters(vreg);
		} 
		catch (FileNotFoundException e) {
			return null;
		}
		return vreg;
	}
}
