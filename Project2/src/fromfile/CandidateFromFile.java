package fromfile;
import java.io.FileNotFoundException;
import java.time.LocalDate;

import election.Election;
import electioncomponents.*;

/**
 * A handler class to retrieve and create Candidate objects from file input.
 * @author Brendan Morgenstern
 *
 */
public class CandidateFromFile extends FromFile<Candidate, Election> {

	public CandidateFromFile(Election e){
		super(e);
		NUMFIELDS = 7;
	}
	public CandidateFromFile(String fileName) throws FileNotFoundException{
		super(fileName);
		NUMFIELDS = 7;
	}
	@Override
	/**
	 * Returns a Candidate object from a given line of text to be read from a file.
	 */
	public Candidate getObjectFromLine(String[] parameters) {
		String last = parameters[0];
		String first = parameters[1];
		LocalDate bday = LocalDate.parse(parameters[2]);
		Boolean citizen = parameters[3].equals("Y");
		String ssn = parameters[4];
		String st = parameters[5];
		Party party = Party.getParty(parameters[6]); 
		return new Candidate(last, first, bday, st, party, citizen, ssn);
	}
	/**
	 * Adds Candidates read from a file into a hashmap.
	 * @param fileName Name of file to be read from.
	 * @param cands The hashmap to add the candidates to
	 */
	public void registerCandidates(String fileName, Election election){
		actOnObjects(fileName, election, (Candidate cand, Election elec)->{
			election.addCandidate(cand);
		});
	}
	/**
	 * Adds Candidates read from a file into a hashmap.
	 * @param cands The hashmap to add the candidates to
	 */
	public void registerCandidates(Election election){
			actOnObjects(election, (Candidate cand, Election elec)->{
				election.addCandidate(cand);
			});
		}
	@Override
	public String getName() {
		return "Candidate";
	}
	@Override
	/**
	 * Checks if the election has candidates registered.
	 */
	public Boolean isEmpty(Election elec) {
		return elec.noCandidates();
	}
	@Override
	public String getDefaultFile(Election election) {
		return "Candidates-" + election.getTypeLabel() + "-" + election.getDateOfElection()+ ".txt";
	}

}
