package fromfile;
import java.io.FileNotFoundException;
import java.time.LocalDate;

import electioncomponents.*;

/**
 * A handler class to retrieve and create Voter objects from file input.
 * @author Brendan Morgenstern
 *
 */
public class VoterFromFile extends FromFile<Voter, VoterRegistry> {
	VoterFromFile(VoterRegistry v){
		super(v);
		NUMFIELDS = 10;
	}
	public VoterFromFile(String fileName) throws FileNotFoundException{
		super(fileName);
		NUMFIELDS = 10;
	}
	@Override
	public Voter getObjectFromLine(String[] parameters) {
		String id = parameters[0];
		String last = parameters[1];
		String first = parameters[2];
		LocalDate bday = LocalDate.parse(parameters[3]);
		LocalDate rday = LocalDate.parse(parameters[4]);
		Party party = Party.getParty(parameters[5]);
		String city = parameters[6];
		String state = parameters[7];
		String zip = parameters[8];
		boolean citizen = parameters[9].equals("Y");
		return new Voter(id, last, first, bday, state, party, citizen, rday, city, zip);
	}

	@Override
	public String getName() {
		return "Voter";
	}
	/**
	 * Registers Voters read in from a file into a VoterRegistry
	 */
	public void registerVoters(String filename, VoterRegistry vreg) {
		actOnObjects(filename, vreg, (Voter voter, VoterRegistry registry)->{
			if(!registry.register(voter, voter.getRegDate()))
				System.err.println(String.format("%s could not be registered", voter.name()));
		});
		if(vreg.empty())
			throw new NullPointerException();
	}
	/**
	 * 
	 * Registers Voters read in from a file into a VoterRegistry
	 */
	public void registerVoters(VoterRegistry vreg) {
		actOnObjects(vreg, (Voter voter, VoterRegistry registry)->{
			if(!registry.register(voter, voter.getRegDate()))
				System.err.println(String.format("%s could not be registered", voter.name()));
		});
	}
	@Override
	/**
	 * Checks if the VoterRegistry has Voters in it.
	 */
	public Boolean isEmpty(VoterRegistry reg) {
		return reg.empty();
	}
	@Override
	public String getDefaultFile(VoterRegistry source) {
		return "Voters.txt";
	}

}
