package fromfile;
/**
 * A simple interface for allowing an object of type O to act on an object of type T.
 * @author Brendan Morgenstern
 * @param <O> The acting object of type O 
 * @param <T> The receiving object, or object being acted on of type T
 */
public interface PerformActionOnObject<O, T> {
	/**
	 * Object X performs an action on Y
	 * @param obj The acting object of type O 
	 * @param target The receiving object, or object being acted on of type T
	 */
	public void action(O obj, T target);
}
