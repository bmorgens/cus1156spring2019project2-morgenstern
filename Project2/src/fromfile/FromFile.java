package fromfile;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.JFileChooser;

/**
 * A handler class to retrieve and create objects from file input.
 * @author Brendan Morgenstern
 * @param <O> The object type to be read from a file.
 * @param <T> The object the objects retrieved from files act on.
 */
public abstract class FromFile<O,T> {
	protected int NUMFIELDS;/*The number of fields the Object to create has; if the number of arguments read from the file doesn't match this number, the format is invalid and */
	protected Scanner fileToRead;
	private Boolean validFile;
	public abstract O getObjectFromLine(String[] line);
	public abstract Boolean isEmpty(T target);
	public abstract String getName();
	public abstract String getDefaultFile(T source);
	FromFile(String fileName){
		try {
			setFile(fileName);
			setValidFile(true);
		} catch (FileNotFoundException e) {
			setValidFile(false);
		}
	}
	FromFile(T src){
		try {
			setFile(getDefaultFile(src));
			setValidFile(true);
		} catch (FileNotFoundException e) {
			setValidFile(false);
		}
	}
	/**
	 * Creates an internal Scanner object to read data from, and asks the user to select one if it can't be found.
	 * @param fileName Name of the file to read from as a String.
	 */
	public void setFile(String fileName) throws FileNotFoundException{
		try {
			fileToRead = new Scanner(new File(fileName));
	    } 
		catch (FileNotFoundException e) {
			try {
				fileToRead = getNewFile(String.format("%s not found, please select a file to read %ss from", fileName, getName()));
				if(fileToRead == null) {
					validFile = false;
					throw new FileNotFoundException();
				}
			}
			catch (Exception ef) {
				throw new FileNotFoundException();
			}
		}
	}
	/**
	 * Performs an action on the given target with a given object.
	 * @param obj The acting object of type O
	 * @param target The object acted upon of type T.
	 * @param perf Action to perform.
	 */
	public void actOnObject(O obj, T target,PerformActionOnObject<O, T> perf) {
		perf.action(obj, target);
	}
	/**
	 * Performs an action on the given target with each O type object created from the file.
	 * @param inputFile Name of the file to read from as a Scanner
	 * @param target Object of type T, which is acted upon.
	 * @param perf Action to perform. 
	 */
	public void actOnObjects(Scanner inputFile, T target, PerformActionOnObject<O, T> perf) throws NullPointerException{
		int lineCount = 1;
		while(inputFile.hasNextLine()) {
			String[] data = inputFile.nextLine().split(",");
			if(data.length != NUMFIELDS)
				System.err.println(String.format("Invalid format on line %d, could not create %s object", lineCount, getName()));
			else
				perf.action(getObjectFromLine(data), target);
			lineCount++;
		}
		if(isEmpty(target))
			throw new NullPointerException();
	}
	/**
	 * 
	 * Performs an action on the given target with each O type object created from the file.
	 * @param filename Name of the file to read from as a String
	 * @param target Object of type T, which is acted upon.
	 * @param perf Action to perform. 
	 */
	public void actOnObjects(String filename, T target, PerformActionOnObject<O, T> perf){
		Scanner inputFile = null;
		try {
			inputFile = new Scanner(new File(filename));
	    } 
		catch (FileNotFoundException e) {
			inputFile = getNewFile(String.format("%s not found, could not create %s object", filename, getName()));
			 if(inputFile == null) {
				 validFile = false;
				 return;
			 }
			 actOnObjects(inputFile, target, perf);
		}
		finally {
			if(inputFile != null)
				inputFile.close();
		}
		return;
	}
	/**
	 * Performs an action on the given target with each O type object created from the file.
	 * @param target Object of type T, which is acted upon.
	 * @param perf Action to perform. 
	 */
	public void actOnObjects(T target, PerformActionOnObject<O, T> perf) throws NullPointerException{
		if(!validFile() && getNewFile(String.format("Please select a file to read in %ss", this.getName())) == null) {
			System.err.println("There is no file to read");
			throw new IllegalStateException();
		}
		try {
			actOnObjects(this.fileToRead, target, perf);
		}
		catch(NullPointerException e) {
			System.err.println(String.format("%s objects could not be created; the file is in an invalid format", this.getName()));
			throw new IllegalStateException();
		}
	}
	/**
	 * Prompts the user to browse for and select a file
	 * @param msg Prompt message
	 * @return Scanner of the selected file, or null if no file was selected
	 */
	public Scanner getNewFile(String msg){
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle(msg);
		int result = fc.showOpenDialog(null);
		if(result == JFileChooser.APPROVE_OPTION)
			try {
				return new Scanner(new File(fc.getSelectedFile().getAbsolutePath()));
			} 
			catch (FileNotFoundException e) {}
			return null;
	} 
	/**
	 * Creates an list of objects of type O from the internal Scanner
	 * @return Returns the ArrayList of objects
	 */
	@SuppressWarnings("unchecked")
	public ArrayList<O> getObjects(){
		ArrayList<O> listOfObjects = new ArrayList<O>();
		actOnObjects((T) listOfObjects, (O obj, T tar)->{
			listOfObjects.add(obj);
		});
		return listOfObjects;
	}
	public Boolean validFile() {
		return validFile;
	}
	public void setValidFile(Boolean validFile) {
		this.validFile = validFile;
	}
}
