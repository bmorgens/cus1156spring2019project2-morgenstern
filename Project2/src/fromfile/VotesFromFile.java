package fromfile;

import java.io.FileNotFoundException;

import election.*;
import electioncomponents.*;

/**
 * 
 * @author Brendan Morgenstern
 *
 */

public class VotesFromFile extends FromFile<Vote, ElectionWithVoterRegistry> {
	public VotesFromFile(String filename) throws FileNotFoundException{
		super(filename);
		NUMFIELDS = 2;
	}
	public VotesFromFile(ElectionWithVoterRegistry e){
		super(e);
		NUMFIELDS = 2;
	}
	@Override
	public Vote getObjectFromLine(String[] line) {
		return new Vote(line[0], line[1]);
	}
	@Override
	public String getName() {
		return "Vote";
	}
	/**
	 * Casts votes read from a file into an election.
	 */
	public void castVotes(ElectionWithVoterRegistry e){
		actOnObjects(e, (vote, election)->{
			election.getElection().vote(vote, election.getRegistry());
		});
	}
	/**
	 * Casts votes read from a file into an election.
	 * @param elec Election to cast votes into
	 * @param vreg Voter Registry
	 */
	public void castVotes(Election elec, VoterRegistry vreg) {
		castVotes(new ElectionWithVoterRegistry(elec, vreg));
	}
	@Override
	/**
	 * Checks if the given election has candidates and votes and the voter registry has voters.
	 */
	public Boolean isEmpty(ElectionWithVoterRegistry target) {
		return (target.getElection().empty() || target.getRegistry().empty());
	}
	@Override
	public String getDefaultFile(ElectionWithVoterRegistry election) {
		return "Votes-" + election.getElection().getTypeLabel() + "-" + election.getElection().getDateOfElection()+ ".txt";
	}
}
