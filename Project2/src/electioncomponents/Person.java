package electioncomponents;
import java.time.LocalDate;

/**
 * A person class to be the base for the various actors in the election.
 * @author Brendan Morgenstern
 *
 */
public class Person {
	protected String lastName;
	protected String firstName;
	protected LocalDate birthday;
	protected String state;
	protected Party party;
	protected boolean citizen;


	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public LocalDate getBirthday() {
		return birthday;
	}
	public void setBirthday(LocalDate birthday) {
		this.birthday = birthday;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Party getParty() {
		return party;
	}
	public void setParty(Party party) {
		this.party = party;
	}
	public boolean isCitizen() {
		return citizen;
	}
	public void setCitizen(boolean citizen) {
		this.citizen = citizen;
	}
	/**
	 * Gives the age of the Person on a given date.
	 * @param date Date to compare with
	 * @return Years between the Person's date of birth and the given date
	 */
	public int getAgeOnDate(LocalDate date) {
		return getBirthday().until(date).getYears();
	}
	public Person(String lastName, String firstName, LocalDate birthday, String state, Party party, boolean citizen) {
		super();
		this.lastName = lastName;
		this.firstName = firstName;
		this.birthday = birthday;
		this.state = state;
		this.party = party;
		this.citizen = citizen;
	}
	/**
	 * Gets a normalized version of the Person's name
	 * @return The Person's name as a String
	 */
	public String name() {
		String first = String.format("%s%s", getFirstName().substring(0, 1).toUpperCase(), getFirstName().substring(1).toLowerCase());
		String last = String.format("%s%s", getLastName().substring(0, 1).toUpperCase(), getLastName().substring(1).toLowerCase());
		return String.format("%s %s", first, last);
	}
	@Override
	public String toString() {
		return name();
	}
}
