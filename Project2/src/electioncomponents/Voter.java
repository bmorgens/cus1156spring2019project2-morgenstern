package electioncomponents;
import java.time.LocalDate;

/**
 * A class representing Voters who will cast votes in the election.
 * @author Brendan Morgenstern
 *
 */
public class Voter extends Person {
	private String id;
	private LocalDate regDate;
	private String town;
	private String zip;
	public Voter(String id, String lastName, String firstName, LocalDate birthday, String state, Party party, boolean citizen, LocalDate regDate, String town, String zip) {
		super(lastName, firstName, birthday, state, party, citizen);
		this.id = id;
		this.regDate = regDate;
		this.town = town;
		this.zip = zip;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public LocalDate getRegDate() {
		return regDate;
	}
	public void setRegDate(LocalDate regDate) {
		this.regDate = regDate;
	}
	public String getTown() {
		return town;
	}
	public void setTown(String town) {
		this.town = town;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	@Override
	public String toString(){
		return String.format("Voter %s ", getId());
	}
	
	
}