package electioncomponents;
 /**
 * A class representing votes to be cast in the election.
 * @author Brendan Morgenstern
 *
 */
public class Vote {
	private String voterID;
	private String SSN;
	public String getVoterID() {
		return voterID;
	}
	public void setVoterID(String voterID) {
		this.voterID = voterID;
	}
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public Vote(String voterID, String sSN) {
		this.voterID = voterID;
		SSN = sSN;
	}
}
