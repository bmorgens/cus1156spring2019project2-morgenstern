package electioncomponents;
public enum Party {
	DEMOCRAT, REPUBLICAN,NONE;
	public enum PartyAbb{D,R}
	/**
	 * Returns a Party object based on an abbreviation 
	 * @param party The party abbreviation
	 * @return The Party related to the given abbreviation
	 */
	private static Party getParty(PartyAbb party){
		return Party.values()[party.ordinal()];
	}
	/**
	 * Returns a Party object associated with the given String, eg. "D" would be a DEMOCRAT.
	 * @param party String representation of the party abbreviation.
	 * @return Party object related to the string or NONE if there's no direct match.
	 */
	public static Party getParty(String party){
		PartyAbb pa;
		try {
			pa = PartyAbb.valueOf(party);
		}
		catch(Exception e){
			return Party.NONE; 
		}
		return getParty(pa);
	}
}