package electioncomponents;
import java.time.LocalDate;


/**
 * A class representing Candidates who will be voted for in the election.
 * @author Brendan Morgenstern
 *
 */
public class Candidate extends Person {
	private int voteCount;
	private String SSN;
	public String getSSN() {
		return SSN;
	}
	public void setSSN(String sSN) {
		SSN = sSN;
	}
	public Candidate(String lname, String fname, LocalDate bday, String state, Party party, Boolean isCitizen, String ssn) {
		super(lname, fname, bday, state, party, isCitizen);
		SSN = ssn;
		voteCount = 0;
	}
	public int getVoteCount() {
		return voteCount;
	}
	public void setVoteCount(int voteCount) {
		this.voteCount = voteCount;
	}
	@Override
	public String toString() {
		return String.format("%s %s has %d votes", getFirstName(), getLastName(), voteCount);
	}
}
