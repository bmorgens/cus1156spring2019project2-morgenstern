package election;

import electioncomponents.*;

/**
 * A container class to hold an election and a registry of voters associated with that election.
 * @author Commander Shepard
 *
 */
public class ElectionWithVoterRegistry {
		private Election election;
		private VoterRegistry vr;
		public VoterRegistry getRegistry() {
			return vr;
		}
		public void setRegistry(VoterRegistry vr) {
			this.vr = vr;
		}
		public Election getElection() {
			return election;
		}
		public void setElection(Election election) {
			this.election = election;
		}
		public ElectionWithVoterRegistry(Election election, VoterRegistry vr) {
			this.election = election;
			this.vr = vr;
		}
		
}
