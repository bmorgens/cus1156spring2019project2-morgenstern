package election;
import java.time.LocalDate;

import electioncomponents.*;
/**
 * An implementation of Election to represent a Primary Election
 * @author Brendan Morgenstern
 *
 */
public class PrimaryElection extends Election {
	private Party party;
	public PrimaryElection(Party partyOfElection, LocalDate dateOf, String stateIn) {
		super(dateOf, stateIn);
		party = partyOfElection;
	}
	@Override
	public String getTypeLabel() {
		return "PRIM";
	}

	@Override
	/**
	 * Tests if a candidate is valid by checking to see if the candidate is of the same party as the election.
	 */
	public Boolean valid(Candidate cand) {
		return cand.getParty().equals(party);
	}
	public Party getParty() {
		return party;
	}

}
