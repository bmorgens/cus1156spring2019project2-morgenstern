package election;
import java.time.LocalDate;

import electioncomponents.Candidate;
/**
 * An implementation of Election to represent a Presidential Election
 * @author Brendan Morgenstern
 *
 */
public class PresidentialElection extends Election {

	private static final int MINIMUMAGE = 35;

	public PresidentialElection(LocalDate dateOf, String stateIn) {
		super(dateOf, stateIn);
	}
	public PresidentialElection(String dateOf, String stateIn) {
		super(LocalDate.parse(dateOf), stateIn);
	}
	@Override
	public String getTypeLabel() {
		return "PRES";
	}
	
	@Override
	/**
	 * Tests if a candidate is valid by checking if they're 35 or older.
	 */
	public Boolean valid(Candidate cand) {
		int age = cand.getAgeOnDate(dateOfElection);
		return (age >= MINIMUMAGE);
	}
}
