package election;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import electioncomponents.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class PresidentialElectionTest {
	private PresidentialElection election;
	private VoterRegistry vreg;
	private Candidate cand1; 
	private Candidate cand2;  
	private Voter voter1;
	@BeforeEach
	void setUp() {
		election = new PresidentialElection(LocalDate.now(), null);
		vreg = new VoterRegistry();
		cand1 = new Candidate("Doe", "John", LocalDate.now().minusYears(40), null, Party.NONE, true, "123-45-6789");
		cand2 = new Candidate("Galt", "John", LocalDate.now().minusYears(55), null, Party.NONE, true, "318-29-6288");
		voter1 = new Voter("OH0023179453","BEARD","MASON",LocalDate.of(1997,04,13), "OH", Party.NONE, true, LocalDate.of(2015,06,30),"SCIO","43988");
	}
	private void addCandidates() {
		if(!election.registerCandidate(cand1))
			System.out.println(String.format("%s could not be added", cand1.name()));
		if(!election.registerCandidate(cand2))
			System.out.println(String.format("%s could not be added", cand2.name()));
	}
	private void addVoter(){
		vreg.register(voter1, voter1.getRegDate());
	}
	@Test
	void testGetTypeLabel() {
		String type = this.election.getTypeLabel();
		assertTrue(type.equals("PRES"));
	}

	@Test
	void testValid() {
		Candidate cand = new Candidate("Doe", "John", LocalDate.now().minusYears(40), null, Party.NONE, true, "123-45-6789");
		assertTrue(election.valid(cand));
		cand.setBirthday(LocalDate.now().minusYears(20));
		assertFalse(election.valid(cand));
	}

	@Test
	void testRegisterCandidate() {
		Candidate cand = new Candidate("Doe", "John", LocalDate.now().minusYears(20), null, Party.NONE, true, "123-45-6789");
		assertFalse(election.registerCandidate(cand));
		assertEquals(election.getCandidates().size(), 0);
		cand.setBirthday(LocalDate.now().minusYears(40));
		assertTrue(election.registerCandidate(cand));
		assertEquals(election.getCandidates().size(), 1);
	}
	@Test
	void testTabulate() {
		addCandidates(); addVoter();
		if(!election.isTabulated()) 
			for(Candidate cand : election.getCandidates().values()) 
				assertEquals(0,cand.getVoteCount());
		//add votes
		Vote voteToCast = new Vote(voter1.getId(), cand1.getSSN());
		election.vote(voteToCast, vreg);
		election.tabulate();
		assertEquals(1, cand1.getVoteCount());
	}

	@Test
	void testVote() {
		addCandidates(); addVoter();
		Vote voteToCast = new Vote(voter1.getId(), cand1.getSSN());
		LocalDate originalRegDate = voter1.getRegDate();
		voter1.setRegDate(LocalDate.now());
		assertEquals(Election.ElectionError.CANDIDATENOTFOUND, election.vote("2", voteToCast.getVoterID(), vreg));
		assertEquals(Election.ElectionError.VOTERNOTFOUND, election.vote(voteToCast.getSSN(), "0", vreg));
		assertEquals(Election.ElectionError.REGISTRYGAP, election.vote(voteToCast.getSSN(), voteToCast.getVoterID(), vreg));
		voter1.setRegDate(originalRegDate);
		assertTrue(election.vote(voteToCast, vreg));
		election.tabulate();
		int votesForCand1 = cand1.getVoteCount();
		assertEquals(1, votesForCand1);
	}

	@Test
	void testGetWinner() {
		addCandidates();
		assertEquals(null, election.getWinner());//not yet tabulated
		election.setTabulated(true);
		for(Candidate cand : election.getCandidates().values())
			cand.setVoteCount(0);
		cand1.setVoteCount(1);
		assertTrue(cand1.getSSN().equals(election.getWinner().getSSN()));
	}
	@Test
	void testTotalNumberOfVotes() {
		addCandidates(); addVoter();
		assertEquals(0, election.totalNumberOfVotes());
		election.vote(cand1.getSSN(), voter1.getId(), vreg);
		election.tabulate();
		assertEquals(1, election.totalNumberOfVotes());
	}

}
