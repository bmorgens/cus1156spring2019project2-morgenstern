package election;

import java.io.FileNotFoundException;

import electioncomponents.VoterRegistry;
import fromfile.*;

/**
 * A thread that will populate an Election object with candidates and votes.
 * @author Brendan Morgenstern
 *
 */
public class ElectionThread extends Thread {
	private ElectionWithVoterRegistry election;
	private Boolean completed;
	private String candFile;
	private String votesFile;
	public ElectionThread(Election election, VoterRegistry registry, String cands, String votes) {
		completed = false;
		this.election = new ElectionWithVoterRegistry(election, registry);
		this.candFile = cands;
		this.votesFile = votes;
	}
	public ElectionThread(Election election, VoterRegistry registry) {
		completed = false;
		this.election = new ElectionWithVoterRegistry(election, registry);
	}

	/**
	 * Adds Candidates read from a file to the election's list of candidates.
	 * @return True if the Candidates were read and added successfully, false otherwise.
	 */
	public Boolean registerCandidates() {
		try {
			if(candFile != null)
				new CandidateFromFile(candFile).registerCandidates(election.getElection());
			else
				new CandidateFromFile(election.getElection()).registerCandidates(election.getElection());
			return true;
		} catch (FileNotFoundException | IllegalStateException ex) {
			return false;
		}
	}
	/**
	 * Adds Votes read from a file to the election's list of votes.
	 * @return True if the Votes were read and added successfully, false otherwise.
	 */
	public Boolean processVotes() {
		try {
			if(votesFile != null)
				new VotesFromFile(votesFile).castVotes(election);
			else
				new VotesFromFile(election).castVotes(election);
			return true;
		} catch (FileNotFoundException e) {
			return false;
		}catch(IllegalStateException ef) {
			return false;
		}
	}
	/**
	 * Checks if the thread was able to populate the election completely.
	 * @return True if the Candidates and Votes were read and added successfully, false otherwise.
	 */
	public Boolean succeeded() {
		return this.completed;
	}
	/**
	 * Populate the election with candidates and votes.
	 */
	public void run() {
		completed =	registerCandidates() && processVotes();
	}
}
