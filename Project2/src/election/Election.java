package election;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import electioncomponents.*;

/**
 * This class facilitates Election data via a list of candidates and a list of votes for said candidates.
 * Can receive votes and determine a winner
 * @author Brendan Morgenstern
 *
 */

public abstract class Election {
	
	protected LocalDate dateOfElection;
	protected String state;
	protected Boolean tabulated;
	protected HashMap<String, Candidate> candidates;
	protected HashMap<String, Integer> votes;
	public Boolean isTabulated() {
		return tabulated;
	}
	public enum ElectionError{NONE, CANDIDATENOTFOUND, VOTERNOTFOUND, REGISTRYGAP};
	public void setTabulated(Boolean tabulated) {
		this.tabulated = tabulated;
	}
	public HashMap<String, Candidate> getCandidates() {
		return candidates;
	}
	public HashMap<String, Integer> getVotes() {
		return votes;
	}
	public LocalDate getDateOfElection() {
		return dateOfElection;
	}
	public void setDateOfElection(LocalDate dateOfElection) {
		this.dateOfElection = dateOfElection;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public Election(LocalDate dateOf, String stateIn) {
		dateOfElection = dateOf;
		state = stateIn;
		tabulated = false;
		candidates = new HashMap<String, Candidate>();
		votes = new HashMap<String, Integer>();
	}
	/**
	 * Returns the type of election.
	 * @return The election type as a String.
	 */
	public abstract String getTypeLabel();
	/**
	 * Determines whether or not a candidate is eligible for this election.
	 * @param cand Candidate to be validated.
	 * @return Returns true if the candidate is eligible, false if not.
	 */
	public abstract Boolean valid(Candidate cand);
	/**
	 * Adds the given candidate to the list of candidates if eligible.
	 * @param cand The candidate to be added.
	 * @return
	 */
	public Boolean registerCandidate(Candidate cand) {
		if(valid(cand)) {
			candidates.put(cand.getSSN(), cand);
			votes.put(cand.getSSN(), votes.getOrDefault(cand.getSSN(), 0));
			return true;
		}
		else
			return false;
	}
	/**
	 * Casts a vote for the given candidate if the vote can be cast.
	 * @param candSSN The candidate's SSN.
	 * @param voterId The voter's ID
	 * @param vreg The registry of voters
	 * @return Returns the particular error if one occurred.
	 */
	public ElectionError vote(String candSSN, String voterId, VoterRegistry vreg){
		Candidate cand = candidates.get(candSSN);
		Voter voter = vreg.find(voterId);
		if(cand == null) 
			return ElectionError.CANDIDATENOTFOUND;
		if(voter == null)
			return ElectionError.VOTERNOTFOUND;
		LocalDate gap = dateOfElection.minusDays(30);
		if(voter.getRegDate().isAfter(gap))
			return ElectionError.REGISTRYGAP;
		else {
			votes.put(candSSN, votes.getOrDefault(candSSN, 0)+1);
			return ElectionError.NONE;
		}
	}
	/**
	 * Casts a vote for the given candidate if the vote can be cast.
	 * @param voteToAdd The Vote with the candidate SSN and voter ID.
	 * @param vreg The registry of voters.
	 * @return Returns true if the vote was cast, or false if an error was encountered.
	 */
	public Boolean vote(Vote voteToAdd, VoterRegistry vreg) {
		return vote(voteToAdd.getSSN(), voteToAdd.getVoterID(), vreg) == ElectionError.NONE;
	}
	/**
	 * Adds a Candidate to the internal list of candidates.
	 * @param cand Candidate to add.
	 */
	public void addCandidate(Candidate cand) {
		candidates.put(cand.getSSN(), cand);
	}
	Iterator<Candidate> getIter(){
		return candidates.values().iterator();
	}
	/**
	 * Determines the winner of the election.
	 * @return Returns the winning candidate or null if no votes were cast. 
	 */
	public Candidate getWinner() {
		if(!tabulated)
			return null;
		return this.getCandidates().values().stream().max((Candidate c1, Candidate c2)-> {
			return c1.getVoteCount() - c2.getVoteCount(); 
		}).get();
		
	}
	/**
	 * Tallies up the number of votes cast for each candidate and adds that vote total to the candidate.
	 */
	public void tabulate() throws IllegalStateException{
		if(noVotes()) {
			throw new IllegalStateException("There are no votes to tabulate");
		}
		else if(noCandidates())
			throw new IllegalStateException("There are no Candidates registered");
		if(tabulated)
			return;
		for(Entry<String, Integer> vote: votes.entrySet())
			candidates.get(vote.getKey()).setVoteCount(vote.getValue());
		tabulated = true;
	}
	/**
	 * Gets the number of total votes cast in this election.
	 * @return The sum of all votes.
	 */
	public int totalNumberOfVotes() {
		int sum = 0;
		for(int count : votes.values())
			sum += count;
		return sum;
	}
	/**
	 * Returns the percent of the total votes a given candidate has.
	 */
	public double getPercentOfTotal(Candidate cand) {
		int vcount = votes.get(cand.getSSN());
		return (vcount*100.0) / totalNumberOfVotes();
	}
	/**
	 * Prints the names of the candidates in this election.
	 */
	public void printCandidates() {
		for(Candidate cand : getCandidates().values())
			System.out.println(cand.name());
	}
	/**
	 * Prints the percentage of the total votes each candidate has
	 */
	public void printCandidateTotals() {
		if(!isTabulated())
			return;
		for(Candidate cand : getCandidates().values()){
			if(cand.getVoteCount() != 0) {
				System.out.println(cand.toString());
				System.out.println(String.format("%.2f%s of the total", getPercentOfTotal(cand), "%"));
			}
		}
	}
	public Boolean noCandidates() {
		return this.getCandidates().values().isEmpty();
	}
	public Boolean noVotes() {
		return this.getVotes().values().isEmpty();
	}
	public boolean empty() {
		return noVotes() && noCandidates();
	}
}
