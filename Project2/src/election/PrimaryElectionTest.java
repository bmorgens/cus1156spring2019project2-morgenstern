package election;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import electioncomponents.Candidate;
import electioncomponents.Party;

class PrimaryElectionTest {
	private PrimaryElection election;
	private Candidate cand;
	
	@BeforeEach
	void setUp() throws Exception {
		election = new PrimaryElection(Party.DEMOCRAT, LocalDate.of(2021, 9, 15),"OH");
		cand = new Candidate("Doe", "John", LocalDate.now().minusYears(40), null, Party.NONE, true, "123-45-6789");
	}

	@Test
	void testGetTypeLabel() {
		assertTrue("PRIM".equals(election.getTypeLabel()));
	}

	@Test
	void testValid() {
		assertFalse(election.valid(cand));
		cand.setParty(election.getParty());
		assertTrue(election.valid(cand));
	}

}
